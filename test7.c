#include <stdio.h>
int main ()
{
    int a,b;
    printf("Enter matrix size\n");
    scanf("%d %d",&a,&b);
    int arr[a][b],brr[b][a],i,j;
    printf("Enter the elements\n");
    for (i=0;i<a;i++)
    {
        for (j=0;j<b;j++)
        {
            scanf("%d",&arr[i][j]);
        }
    }
            
    for (i=0;i<a;i++)
    {
        for (j=0;j<b;j++)
        {
            brr[j][i]=arr[i][j];
        }
    }
    printf("The given matrix is:\n");
    for (i=0;i<a;i++)
    {
        for (j=0;j<b;j++)
        {
            printf ("%d\t",arr[i][j]);
        }
        printf ("\n");
    }
    printf("Transpose of given matrix is:\n");
    for(i=0;i<b;i++)
    {
        for (j=0;j<a;j++)
        {
            printf("%d\t",brr[i][j]);
        }
        printf("\n");
    }
    return 0;
}

