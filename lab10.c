#include <stdio.h>
int main()
{
    FILE *fp;
    char ch;
    fp = fopen("Input.txt", "w");
    printf("Enter data\n");
    while((ch=getchar()) != EOF)
    {
        putc(ch,fp);
    }
    fclose(fp);
    fp = fopen("Input.txt","r");
    printf("\nYour file content is:\n\n\n");
    while ((ch=getc(fp)) != EOF)
    {
        printf("%c",ch);
    }
    fclose(fp);
    return 0;
}